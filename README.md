HAB Venting & Ballasting Calcuator
=====================================

A calculator for determining how long to vent lift gas or drop ballast for to achieve target ascent rates

To run
-------------------------------------
Open /calculator/index.html in a browser. The calculator is designed
to work locally, without an Internet connection.

To change the calculator's default values, edit /calculator/defaults.js


Developer Notes
-------------------------------------
Tests are in /test and use Node.js with Mocha. To test:

    cd test
    npm install
    npm test

License
-------------------------------------
Released under the standard MIT license. This code is free to use, distribute,
and modify. Please see the license file for complete details.


Authors
-------------------------------------
Originally created by Phil Manijak in Corvallis, Oregon for Smith & Williamson LLC 
Modified by ___ for Balloon Ascent Technologies LLC

var defaults;
$(function () {
    defaults = {
        // Calculations
        targAscentRate1: 3.0,
        // Ascent targets:
        initialAscentRate: 4.5, // m/s
        ballastDropInSeconds: 0, // s

        // Flight system properties:
        envelopeMass: 300,      // g
        flightSystemMass: 2000, // flight system & payload, g
        ballastMass: 1000,       // g
        balloonDrag: 0.25,
        neckTubeInletDiameter: 6, // cm
        rSpecificLiftingGas: 2077, // J/kg K
        ballastReleaseRate: 8.0,  // g/s

        // Launch site conditions:
        lsPressure: 1010, // hPa
        lsTemperature: 10, // deg C
        lsBalloonDiffPressure: 133, // hPa
        lsBalloonGasTemperature: 10, // deg C

        // Target altitude conditions:  
        ambientPressure1: 103, // hPa
        ambientTemperature1: -52, // deg C
        ambientBalloonDiffPressure: 100, // hPa
        ambientBalloonGasTemperature: -50, // deg C

        balloonSizeData: `Size, Mass (g), Cd, Diff Pressure (Pa), Burst Diameter (m), Barely Inflated Diameter (m)
        Totex 100,100,0.28,200,1.96,0.340
        Totex 200,200,0.28,200,3,0.550
        Totex 300,300,0.28,150,3.78,0.690
        Totex 350,400,0.28,150,4.12,0.750
        Totex 450,450,0.28,150,4.72,0.860
        Totex 500,500,0.28,150,4.99,0.910
        Totex 600,600,0.28,130,6.02,1.000
        Totex 700,700,0.28,130,6.53,1.090
        Totex 800,800,0.28,130,7,1.170
        Totex 1000,1000,0.28,130,7.86,1.310
        Totex 1200,1200,0.28,120,8.63,1.440
        Totex 1500,1500,0.28,110,9.44,1.610
        Totex 2000,2000,0.28,100,10.54,1.840
        Totex 3000,3000,0.28,100,13,2.270
        Hwoyee 100,100,0.28,200,1.8,
        Hwoyee 200,200,0.28,200,2.9,
        Hwoyee 300,300,0.28,150,3.8,
        Hwoyee 350,350,0.28,150,4.1,
        Hwoyee 500,500,0.28,150,5,
        Hwoyee 600,600,0.28,130,5.8,
        Hwoyee 800,800,0.28,130,6.8,
        Hwoyee 1000,1000,0.28,120,7.5,
        Hwoyee 1200,1200,0.28,120,8.5,
        Hwoyee 1600,1600,0.28,110,10.5,
        Hwoyee 2000,2000,0.28,100,11,
        Hwoyee 3000,3000,0.28,100,12,
        `
    };

    for (var prop in defaults) {
        var val = defaults[prop];
        $('#' + prop).val(val);
    }
});
var debug = true;

var vehicle;    // Struct to hold all of the flight system parameters

var balloonTypes = [
    "<option value=Main>Main</option>",
    "<option value=Tow>Tow</option>",
    "<option value=Vent>Vent</option>"
    ];
var balloonSizeData = [];  // Data will be loaded here
var balloonSizes = "";
var balloonGas = [
    "<option value='Helium'>Helium</option>",
    "<option value=Hydrogen>Hydrogen</option>",
    "<option value=Methane>Methane</option>"
    ];

var selectedBalloonType = []; 
var selectedBalloonSize = []; 
var selectedBalloonGas = []; 

var calcFields = [
    "ambientPressure",
    "ambientTemperature",
    "targAscentRate"
];
var outputFields = [
    "ventTime",
    "ballastTime",
    "ballastRemaining"
]

var constants = {
    BalloonDrag: 0.25,            // coefficient
    ValveTubeInletDiameter: 0.06, // m

    HeliumDensity: 0.1786, // ... at 20C, 101 kPa (kg/m^3)
    HydrogenDensity: 0.0827, // ... at 20C, 101 kPa (kg/m^3)
    AirDensity: 1.2930,    // ... at 20C, 101 kPa (kg/m^3)
    RSpecificHe: 2077,     // J/Kg K    see https://www.engineeringtoolbox.com/individual-universal-gas-constant-d_588.html
    RSpecificH : 4124,
    RSpecificCH4: 518,
    RSpecificAir: 287,   // J/Kg K
    Gravity: 9.810,         // m/s^2

    RadiusEarth: 6369,          // Radius of the Earth, in km
        // US Standard Atmosphere (1976) constants
    StdAtmos_minAlt: [0, 11, 20, 32, 47, 51, 71, 85],                                  // Zone min altitude, in km
    StdAtmos_temp: [288.15, 216.65, 216.65, 228.65, 270.65, 270.65, 214.65, 186.95],   // Temp at bottom of each zone, in K
    StdAtmos_Tgrad: [-6.5, 0.0, 1.0, 2.8, 0.0, -2.8, -2.0, 0]                          // Temp-grad for each zone, in K/km
};

var error = function (message) {
    var err = new Error(message);
    throw err;
};

var updateView = function (colNum) {
    // ---- Parse user entries
    var targetAscentRate = parseFloat($("#targAscentRate" + colNum).val());
    var targetPressure = parseFloat($("#ambientPressure" + colNum).val()) * 100;
    var targetTemperature = parseFloat($("#ambientTemperature" + colNum).val()) + 273;

    // ---- Verify Parameters
    if (!targetPressure || targetPressure <= 0.0) {
        error("Maneuver pressure must be greater than 0 Pa");
    }
    
    if (!targetTemperature || targetTemperature <= 0.0) {
        error("Maneuver temperature must be greater than 0 K");
    }    
    
    // ---- Calculate Values
    var maneuverResults = calcManeuver(targetPressure, targetTemperature, targetAscentRate);

    // ---- Display Results
    
    $("#ballastRemaining" + colNum).text(vehicle.mass.ballast.toFixed(0));

    $("#ballastTime" + colNum).text(maneuverResults.ballastTime.toFixed(0));
    $("#ventTime" + colNum).text(maneuverResults.ventTime.toFixed(0))
    setTimeDisplay(colNum);
    // $("#lsAirDensity").text(calc.initial.airDensity.toFixed(4));
    // $("#ambientAirDensity").text(calc.neutral.airDensity.toFixed(4));
};

function setTimeDisplay(colNum) {
    if (
        parseFloat($("#ventTime" + colNum).text()) == 0
    ) {
        $("#ventTimeRow .col" + colNum).css('opacity', '15%');
    } else {
        $("#ventTimeRow .col" + colNum).css('opacity', '100%');
    }
    if (
        parseFloat($("#ballastTime" + colNum).text()) == 0
    ) {
        $("#ballastTimeRow .col" + colNum).css('opacity', '15%');
    } else {
        $("#ballastTimeRow .col" + colNum).css('opacity', '100%');
    }
}

$(function () {
    var fieldsToUpdateOnChange = [
        'ambientPressure1',
        'ambientTemperature1',
        'targAscentRate1',

        'envelopeMass',
        'flightSystemMass',
        'ballastMass',

        'neckTubeInletDiameter',
        'balloonDrag',
        'rSpecificLiftingGas',
        'ballastReleaseRate',

        'lsPressure',
        'lsTemperature',
        'lsBalloonDiffPressure',
        'lsBalloonGasTemperature',

        'initialAscentRate',
        'ballastDropInSeconds',

        
        'ambientBalloonDiffPressure',
        'ambientBalloonGasTemperature'
    ];
    for (var index in fieldsToUpdateOnChange) {
        $("#" + fieldsToUpdateOnChange[index] + ", input[name='liftGas']").change(function() {
            updateAll();            
        });
    }
    // For GET reqs
    // $.ajax({
    //     type: "GET",
    //     url: "balloon_data.csv",
    //     dataType: "text",
    //     success: function(data) {setballoonSizeSelector(data)}
    // });
    setBalloonSelector(defaults.balloonSizeData); // Comment this out if data is fetched
    updateAll();
});

// ==== Function to handle parsing, checking, and calculating initial Flight System Parameters ==== //
var SetFlightSystemParameters = function () {

    // ---- Parse user entries
    rSpecificLiftingGas = parseFloat($("input[name='liftGas']:checked").val());

    // const balloonSizeSelected = document.getElementById("balloonSize1");
    // // console.log(balloonSizeSelected.options[balloonSizeSelected.selectedIndex].value)
    // console.log(balloonSizeSelected.selectedIndex)
    // balloonSizeData.find(balloon => balloon.balloonSize === balloonSizeSelected);
    // console.log(balloonSizeData[2])

    vehicle = {
        mass: {
            gas: 0,                                                                     // g, To be calculated and upated later
            balloon: parseFloat($("#envelopeMass").val()) || 111,                              // g
            payload: parseFloat($("#flightSystemMass").val()) || 222,                          // g
            ballast: parseFloat($("#ballastMass").val()) || 333                                // g
        },
        balloon: {
            drag: parseFloat($("#balloonDrag").val()) || 0.25,                                  // Cd
            diffPressure: parseFloat($("#ambientBalloonDiffPressure").val()) || 105,           // Pa
        },
        rSpecificLiftingGas: rSpecificLiftingGas || constants.RSpecificHe,                                       // rSpecific
        ballastReleaseRate: parseFloat($("#ballastReleaseRate").val()) || 8,                 // g/s
        valve: {
            neckTubeInletDiameter: parseFloat($("#neckTubeInletDiameter").val()) / 100 || 0.05 // m
        },
    };

    console.log(selectedBalloonSize);

    // ---- Verify Parameters
    if (!vehicle.mass.balloon || vehicle.mass.balloon <= 0.0) {
        error("Balloon mass must be greater than 0 g");
    }

    if (!vehicle.mass.payload || vehicle.mass.payload <= 0.0) {
        error("Payload mass must be greater than 0 g");
    }

    if (!vehicle.balloon.drag || vehicle.balloon.drag < 0.0 || vehicle.balloon.drag > 1.0) {
        error("Balloon drag (Cd) must be between 0 and 1");
    }

    if (!vehicle.balloon.diffPressure || vehicle.balloon.diffPressure < 0.0) {
        error("Balloon diff pressure must be greater than 0 Pa");
    }
    
    if (!vehicle.ballastReleaseRate || vehicle.ballastReleaseRate <= 0.0) {
        error("Ballast release rate must be greater than 0 g/s");
    }

    if (!vehicle.valve.neckTubeInletDiameter || vehicle.valve.neckTubeInletDiameter <= 0.0) {
        error("Vent diameter must be greater than 0 cm");
    }

    // ---- Calculate Values
    var totalLaunchMass = vehicle.mass.balloon +
            vehicle.mass.payload +
            vehicle.mass.ballast; // g

    var launchAltitude = (1 - Math.pow(((parseFloat($("#lsPressure").val())) / 1013.25), 0.190284)) * 145366.45 * 0.3048;

    // ---- Display Results
    $("#totalMass").text(totalLaunchMass);
    $("#altitude").text(launchAltitude.toFixed(0));
}

// ==== Function to handle parsing, checking, and calculating initial Launch Parameters ==== //
var SetLaunchParameters = function () {
    // ---- Parse user entries
    var launch = {
        ascentRate: parseFloat($("#initialAscentRate").val()),                          // m/s
        pressure: parseFloat($("#lsPressure").val()) * 100,                             // Pa
        temperature: parseFloat($("#lsTemperature").val()) + 273,                       // K
        balloon: {
            diffPressure: parseFloat($("#ambientBalloonDiffPressure").val()),           // Pa
            gasTemperature: parseFloat($("#lsTemperature").val()) + 273,                // K
        }
    }

    // ---- Verify Parameters
    if (!launch.pressure || launch.pressure <= 0.0) {
        error("Launch site pressure must be greater than 0 Pa");
    }

    if (!launch.balloon.gasTemperature || launch.balloon.gasTemperature < 0.0) {
        error("Launch site temperature must be greater than 0 K");
    }

    // ---- Calculate Values
    var launchState = calcTargetState(launch, launch.ascentRate);
    vehicle.mass.gas = launchState.gasMass;

    // ---- Display Results
    $("#launchVolume").text(launchState.liftGasVolume.toFixed(2));                      // g
    $("#freeLiftg").text(launchState.freeLift_g.toFixed(0));                            // g
}


function setBalloonSelector(data) {
    data.split("\n").forEach((eachBalloon, index) => {
        if (index === 0) return;                                                        // Data Heading

        let eachData = eachBalloon.split(",");
        if (eachData[0] === "") return;

        let dataObj = {
            balloonSize: eachData[0].trim(),
            burstDiameter: parseFloat(eachData[1]),
            barelyInflatedDiameter: parseFloat(eachData[2])
        };
        balloonSizeData.push(dataObj);
    });

    balloonSizeData.forEach(balloonSize => {
        balloonSizes += `<option value="${balloonSize.balloonSize}">${balloonSize.balloonSize}</option>\n`
    })

    for (i = 1; i < 7; i++) {
        $("#balloonType" + i).html(balloonTypes);
        $("#balloonSize" + i).html(balloonSizes);
        $("#balloonGas" + i).html(balloonGas);

        // $("#balloonType" + i + " option").click(function() {
        //     selectedBalloonType[i] = $(this).val();
        //     console.log(selectedBalloonType[i]);
        // })

        // $("#balloonSize" + i + " option").click(function() {
        //     selectedBalloonSize[i] = balloonSizeData.find(balloon => balloon.balloonSize === $(this).val());
        //     console.log(selectedBalloonSize[i]);
        // })

        // $("#balloonGas" + i + " option").click(function() {
        //     selectedBalloonGas[i] = $(this).val();
        //     console.log(selectedBalloonGas[i]);
        // })        
    }
} 

function updateAll() {
    SetFlightSystemParameters();
    SetLaunchParameters();

    for (var columnNum = 1; columnNum <= $(".maneuver").length; columnNum++) {
        updateView(columnNum);
    }
}

function addBalloon() {
    var balloonCount = $(".balloon").length;
    if (balloonCount < 6) {
        $("#balloons #heading .balloon").last().next().addClass("balloon");
        var colNum = balloonCount + 1;
        $(".bCol" + colNum).css('display', 'table-cell');
    }
}
function removeBalloon() {
    var balloonCount = $(".balloon").length;
    if (balloonCount > 1) {
        $("#balloons #heading .balloon").last().removeClass("balloon");
        $(".bCol" + balloonCount).css('display', 'none');
    }
}

function addManeuver() {
    var maneuverCount = $(".maneuver").length;
    if (maneuverCount < 6) {
        $("#maneuvers #heading .maneuver").last().next().addClass("maneuver");
        var colNum = maneuverCount + 1;
        $(".col" + colNum).css('display', 'table-cell');
        for (i in calcFields) {
            if (calcFields[i] === "targAscentRate") {
                $("#targAscentRate" + colNum).val((parseFloat($("#targAscentRate" + maneuverCount).val()) + 0.1).toFixed(1));
            } else {
                $("#" + calcFields[i] + colNum).val(parseFloat($("#" + calcFields[i] + maneuverCount).val()));
            }
            $("#" + calcFields[i] + colNum).change(function() {
                updateAll();
            })
            
        }
        updateAll();
    }
}
function removeManeuver() {
    var maneuverCount = $(".maneuver").length;
    if (maneuverCount > 1) {
        $("#maneuvers #heading .maneuver").last().removeClass("maneuver");
        $(".col" + maneuverCount).css('display', 'none');
    }
}


// ==== Calculate the system's ascent rate give the ambient parameters and liftgas mass and overall system mass ==== //
var calculateState = function (input, gasMass, vehicleMass_g) {
    // if (!input.pressure || !input.temperature || !input.balloon.diffPressure || !gasMass || !vehicleMass_g) {
    //     error("Insuficient input terms for calculateState()");
    // }

    var airDensity = input.pressure / (constants.RSpecificAir * input.temperature);
    var balloonGaugePressure = input.pressure + input.balloon.diffPressure;             // Pa
    
        // Use ideal gas law to determine volume of lift gas (P * V = m * Rspec * T)
    var liftGasDensity = balloonGaugePressure / (rSpecificLiftingGas * input.balloon.gasTemperature);
    var liftGasVolume = gasMass / liftGasDensity;
    var liftGasRadius = Math.pow((3 * liftGasVolume) / (4 * Math.PI), 1/3);
    if (liftGasRadius <= 0.0) { error("Gas mass cannot be zero, if you want a positive ascent rate."); }

        // Calculate buoyancy based on volume of air displaced by lift-gas
    var grossLift_kg = liftGasVolume * (airDensity - liftGasDensity);
    if (grossLift_kg <= 0) { error("The balloon gas density is more dense than air. It is impossible to ascend."); }

        // Calculate free-lift from buoyancy
    var freeLift_kg = grossLift_kg - (vehicleMass_g / 1000);
    var freeLiftNewtons = freeLift_kg * constants.Gravity;

        // Save if free-lift is positive or negavite, avoiding imaginary numbers
    var negative;
    if (freeLiftNewtons <= 0) { negative = true; }

        // Calculate velocity based on buoyancy and drag
    var ascentRate = Math.sqrt(Math.abs(freeLiftNewtons) / 
                    (0.5 * vehicle.balloon.drag * airDensity * (Math.PI * Math.pow(liftGasRadius,2))));

    return {
        ascentRate: negative? ascentRate * (-1) : ascentRate,
        freeLift_g: freeLift_kg * 1000,
        liftGasVolume: liftGasVolume,
    };
};

// ==== Calculate new amount of liftgas or ballast mass to reach a target ascent rate ==== //
var calcTargetState = function (input, ascentRateTarget, gasMassInput, accuracy) {      // accuracy ~~ iterations. More accuracy = Slower 
    var counter = 0;
    var gasMass, state;

    var vehicleMass = vehicle.mass.balloon + vehicle.mass.payload + vehicle.mass.ballast;

        // --- Trying to figure out how much gas we need to have (launch or venting)
    if (!gasMassInput) {
        gasMass = 0.0010;
        var massIsGreaterThan = 0.0;
        var massIsLessThan = undefined;

        do {
            state = calculateState(input, gasMass, vehicleMass);

            if (state.ascentRate < ascentRateTarget) { massIsGreaterThan = gasMass; }   // The target mass is greater than where we are at
            else { massIsLessThan = gasMass; }                                          // We've overshot our ascent target

            if (!massIsLessThan) { gasMass = gasMass * 2; }                             // If we don't know our max yet, double the guess
            else { gasMass = (massIsGreaterThan + massIsLessThan) / 2; }                // Otherwise guess halfway between things

            if (++counter > 2000) {
                console.error("Too many calculateState() calls");
                return;
            }; // Overflow protection
        } while (Math.abs(state.ascentRate - ascentRateTarget) > (accuracy || 0.0001))
        
        // --- Trying to figure out how much ballast we need to have (ballasting)
    } else {
        var maxMass = vehicleMass;
        var minMass = undefined;
        
        do {
            state = calculateState(input, gasMassInput, vehicleMass);

            if (state.ascentRate > ascentRateTarget) { minMass = vehicleMass; }     // The target mass is less than where we are at
            else { maxMass = vehicleMass; }                                         // We've overshot our ascent target

            if (!minMass) { vehicleMass = vehicleMass / 2; }                        // If we don't know our max yet, double the guess
            else { vehicleMass = (minMass + maxMass) / 2; }                         // Otherwise guess halfway between things

            if (++counter > 2000) {
                console.error("Too many calculateState() calls");
                return;
            }; // Overflow protection
        } while (Math.abs(state.ascentRate - ascentRateTarget) > (accuracy || 0.0001) && vehicleMass > 0);
    }

    return {
        vehicleMass_g: vehicleMass, 
        gasMass: gasMass,
        liftGasVolume: state.liftGasVolume,
        freeLift_g: state.freeLift_g,
    };
};

// ==== Calculates venting duration for a given gas mass change and valve size, based on ambient pressure ==== //
var calcVentTime = function (input, initialMass, targetMass) {
    if (!input.pressure || ! input.balloon.diffPressure || !input.balloon.gasTemperature || !initialMass || !targetMass) {
        error("Insuficient input terms for calcVentTime()");
    }

    var balloonGaugePressure = input.pressure + input.balloon.diffPressure;             // Pa
    var liftGasDensity = balloonGaugePressure / (rSpecificLiftingGas * input.balloon.gasTemperature);

    var idealOutletVelocity = Math.sqrt((2 * input.balloon.diffPressure) / liftGasDensity); 
    var neckTubeInletArea = Math.PI * Math.pow((vehicle.valve.neckTubeInletDiameter/2), 2);
    var volumetricFlowRate = idealOutletVelocity * neckTubeInletArea;
    var massFlowRate = volumetricFlowRate * liftGasDensity;
    var ventTime = (initialMass - targetMass) / massFlowRate;

    vehicle.mass.gas = targetMass;                                                      // Update vehicle struct

    return ventTime;
};

// ==== Calculates ballasting duration for a given system mass change and ballaster rate ==== //
var calcBallastTime = function (initialMass, targetMass) {
    if (!initialMass || !targetMass) {
        error("Insuficient input terms for calcVentTime()");
    }    

    // if ( targetMass < vehicle.mass.balloon+vehicle.mass.payload ) {
    //     console.error("Insufficient Ballast");
    //     // TODO Change ballast remaining field red or something more useful to alert user they're out
    // }
    var ballastAmount = initialMass - targetMass;
    var ballastTime = ballastAmount / vehicle.ballastReleaseRate;
    
    vehicle.mass.ballast -= ballastAmount;                                              // Update vehicle struct

    return ballastTime;
};

// ==== Determines what action is needed to reach a target ascent rate ==== //
var calcManeuver = function (targetPressure, targetTemperature, targetAscentRate) {
    var targetState;
    var ventTime = 0;
    var ballastTime = 0;
    var logger = {};

    if (!targetPressure || !targetTemperature) {
        error("Insuficient input terms for calcManeuver()");
    }       

    var maneuver = {
        ascentRate: 0,                                                                  // m/s, update shortly
        pressure: targetPressure,                                                       // Pa
        temperature: targetTemperature,                                                 // K
        balloon: {
            diffPressure: vehicle.balloon.diffPressure,                                 // Pa, may change as function of inflation at some point
            gasTemperature: targetTemperature,                                          // K, may change as function of solar heatinga t some point
        },
        mass: {
            total: vehicle.mass.balloon + vehicle.mass.payload + vehicle.mass.ballast,
            ballast: vehicle.mass.ballast,
            gas: vehicle.mass.gas
        }
    }

    // --- Calculate current expected ascent rate going into the maneuver
    var initialState = calculateState(maneuver, maneuver.mass.gas, maneuver.mass.total);

    // ---- Calculate target state
    if (initialState.ascentRate > targetAscentRate) {
        if (debug) { console.log("\n --- Venting Maneuver ---"); }
        targetState = calcTargetState(maneuver, targetAscentRate);
        ventTime = calcVentTime(maneuver, maneuver.mass.gas, targetState.gasMass);
    } else {
        if (debug) { console.log("\n --- Ballasting Maneuver ---"); }
        targetState = calcTargetState(maneuver, targetAscentRate, vehicle.mass.gas);
        ballastTime = calcBallastTime(maneuver.mass.total, targetState.vehicleMass_g)
    }

    if (debug) {
    logger["Ascent Rate"] = initialState.ascentRate.toFixed(1) + "  ==> "  + targetAscentRate.toFixed(1) + " m/s";       
    logger["Free Lift"] = (initialState.freeLift_g).toFixed(0) + "  ==>  " + (targetState.freeLift_g).toFixed(0) + " g";
    logger["Gas mass"] = (maneuver.mass.gas * 1000).toFixed(0) + " - "  + ((maneuver.mass.gas - vehicle.mass.gas) * 1000).toFixed(0) + " = " +
                            (vehicle.mass.gas * 1000).toFixed(0) + " g";
    logger["Ballast mass"] = maneuver.mass.ballast.toFixed(0) + " - "  + (maneuver.mass.total-targetState.vehicleMass_g).toFixed(0) + " = " +
                            vehicle.mass.ballast.toFixed(0) + " g";

    console.table(logger)
    }

    return {
        ventTime: ventTime,
        ballastTime: ballastTime,
        initialAscentRate: initialState.ascentRate
    }
}

// for Node.js (unit testing)
if (typeof(window) === 'undefined' && module) {
    module.exports = SAW.test;
}
